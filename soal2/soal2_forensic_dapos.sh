
#!/bin/bash

path=forensic_log_website_daffainfo_log
log=~/log_website_daffainfo.log
avg=ratarata.txt
res=result.txt

if ! [[ -d $path ]]; then
	mkdir $path
fi
if ! [[ -f $path/$avg ]]; then
	touch $path/$avg
fi
if ! [[ -f $path/$res ]]; then
	touch $path/$res
fi

awk 'BEGIN{c1=0} 
//{c1++} 
END{print "Rata-rata serangan adalah sebanyak ",(c1-1)/12,"requests per jam"}' $log > $path/$avg

max_ip=$(awk -F: 'NR!=1{if($1)a[$1]++}END{for (i in a) {print i, a[i]}}' $log | sort -k 2n | tail -1 | awk '{print "IP yang paling banyak mengakses server adalah: "$1" sebanyak "$2" requests"}')
curl_req+=$(awk '/curl/ {++n}END {print "ada", n ,"requests yang menggunakan curl sebagai user-agent\n"}' $log)
twoam_ip+=$(awk -F:  '/22[/]Jan/{if($3=="02") print $1}' $log)
echo -e "$max_ip\n\n$curl_req\n\n$twoam_ip" > $path/$res
