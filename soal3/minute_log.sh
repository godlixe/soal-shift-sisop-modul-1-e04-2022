#!/bin/bash

log=~/log

if ! crontab -l | grep -q '~/soal-shift-sisop-modul-1-e04-2022/soal3/minute_log.sh'; then
	(crontab -l; echo "* * * * * ~/soal-shift-sisop-modul-1-e04-2022/soal3/minute_log.sh") | crontab -
	echo "not exists"
fi

if ! [[ -d $log ]]; then
	mkdir $log
fi

path_size=$(du -sh $log | awk '{print  $1}')
path_log="$log/metrics_$(date +"%Y%m%d%H%M%S").log"

chmod +x ~/soal-shift-sisop-modul-1-e04-2022/soal3/minute_log.sh
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
$(free -m | awk 'NR == 2 {printf "%d,%d,%d,%d,%d,%d,", $2, $3, $4, $5, $6, $7} NR == 3 {printf "%d,%d,%d,%d,", $2, $3, $4, $5}')$log,$path_size" > $path_log

find $log -type f -exec chmod 600 {} \;
