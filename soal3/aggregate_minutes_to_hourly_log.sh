#!/bin/bash

log=~/log

if ! crontab -l | grep -q 'aggregate_minutes_to_hourly_log.sh'; then
        (crontab -l; echo "0 * * * * ~/soal-shift-sisop-modul-1-e04-2022/soal3/aggregate_minutes_to_hourly_log.sh") | crontab -
fi

file_name="metrics_agg_$(date +"%Y%m%d%H").log"
t="metrics_$(date +%Y%m%d)"
h="$(date +%H)"
h=$((h-1))
if [[ $h -lt 0 ]]; then
	h+=24;
	h=$((h%24))
elif [[ $h -lt 10 ]]; then
	t+="0$h"
else
	t+=$h
fi

for i in {1..12}
do
	max[$i]=$(awk -F',' -v i=$i 'NR%2==0{print $i}' $log/$t*.log | sort -r | head -1)
	min[$i]=$(awk -F',' -v i=$i 'NR%2==0{print $i}' $log/$t*.log | sort | head -1)
	avg[$i]=$(awk -F',' -v i=$i 'NR%2==0{sum+=$i} END{print 2*sum/NR}' $log/$t*.log)
done

avg[11]=$log
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
minimum,$(printf '%s,' "${min[@]}")
maximum,$(printf '%s,' "${max[@]}")
average,$(printf '%s,' "${avg[@]}")" > "$log/$file_name"

find $log -type f -exec chmod 600 {} \;
