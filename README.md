# Penjelasan soal-shift-sisop-modul-1-e04-2022

1. Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun,
karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan
senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk
mempermudah pekerjaan mereka, Han membuat sebuah program.
- Han membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login
yang dibuat di script main.sh
- Demi menjaga keamanan, input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format :
MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi
yang dilakukan user.
    - Ketika mencoba register dengan username yang sudah terdaftar, maka
message pada log adalah REGISTER: ERROR User already exists
    - Ketika percobaan register berhasil, maka message pada log adalah REGISTER:
INFO User USERNAME registered successfully
    - Ketika user mencoba login namun passwordnya salah, maka message pada
log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User
USERNAME logged in

- Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai
berikut :
    - dl N ( N = Jumlah gambar yang akan didownload)
        Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan
        jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan
        dimasukkan ke dalam folder dengan format nama
        YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki
        format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01,
        PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di
        zip dengan format nama yang sama dengan folder dan dipassword sesuai
        dengan password user tersebut. Apabila sudah terdapat file zip dengan
        nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu,
        barulah mulai ditambahkan gambar yang baru, kemudian folder di zip
        kembali dengan password sesuai dengan user.
    - att
        Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
        user yang sedang login saat ini.

### Jawaban

Untuk script register.sh

Mula-mula, path untuk log dan file database user dicek dulu. Apabila tidak ada, script akan membuat file tersebut. File log akan digunakan untuk menyimpan 

```bash
log=log.txt
db=users/user.txt

#checking if log file exists
if ! [[ -f "$log" ]]; then
        touch "$log"
fi
#checking if db exists
if ! [[ -f "$db" ]]; then
        mkdir users
        touch "$db"
fi
```
#### Isi folder setelah script dijalankan :
![img](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-12-35.png)

Untuk tanggal pada format pengisian log, digunakan command date sebagai berikut.

```bash
msg="$(date +%D" "%T) "
```

Username dan password kemudian dibaca dari input user, apabila password tidak sesuai dengan ketentuan maka script akan menjalankan loop sampai password sesuai dengan ketentuan. Input password menggunakan `read -s` agar tidak terlihat. Digunakan gabungan regex untuk memvalidasi password. Kesulitan yang didapati adalah saat mencari cara agar regex sesuai dengan persyaratan pada soal.

```bash
echo "Input username to register : "
read  username

while ! [[ "$password" =~ [[:alnum:]] && ${#password} -ge  8 && "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]]  && ($password != $username)>
do
        echo "Enter an 8+ character of alphanumeric password"
        read -s password
done

if grep -q "^$username" $db ; then
        msg+="REGISTER: ERROR User already exists"
else
        echo "$username:$password" >> $db
        msg+="REGISTER: INFO User $username registered successfully"

fi

```
#### Looping input password :

![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-32-32.png)

Apabila username yang dicari sudah terdaftar, maka message log akan berisi REGISTER: ERROR User already exists. Apabila username berhasil didaftarkan, message log akan berisi REGISTER: INFO User $username registered successfully. Digunakan grep untuk mencari username pada database user. Message untuk log kemudian di-echo ke dalam log.

```bash
if grep -q "^$username" $db ; then
        msg+="REGISTER: ERROR User already exists"
else
        echo "$username:$password" >> $db
        msg+="REGISTER: INFO User $username registered successfully"

fi

echo "$msg" >> $log
```
#### Tampilan gagal register :
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-36-15.png)

Apabila username berhasil didaftarkan, username dan password yang terkait akan disimpan juga di dalam database

#### Isi database :
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-49-43.png)
#### Isi log :
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-48-52.png)

Untuk script main.sh

Mula-mula, dilakukan pengecekan file log dan database kembali. Apabila file tidak ada, maka script akan membuat file tersebut. Script kemudian meminta user untuk mengetikkan password. Input password menggunakan `read -s` agar tidak terlihat.

Untuk mencari dan mencocokan username dengan password yang diketik, digunakan awk. Apabila password tidak sesuai, message untuk log akan berupa LOGIN: ERROR Failed login attempt on user $username. Apabila berhasil login, LOGIN: INFO User $username logged in. Dibuat juga variabel success sebagai penanda apabila user berhasil login, maka success akan bernilai 1, apabila tidak akan bernilai 0 dan program akan exit setelah script mendata kegagalan login ke log.

```bash
db_password=`awk -v name="$username" -F: '$1 ~ name {print $2}' $db`

if ! [[ $db_password == $password ]]; then
        msg+="LOGIN: ERROR Failed login attempt on user $username"
else
        msg+="LOGIN: INFO User $username logged in"
        success=1
fi

echo "$msg" >> $log
echo "$msg"

if [[ $success == 0 ]]; then
        exit
fi
```

Apabila berhasil login, program akan meminta input user untuk perintah selanjutnya. Apabila user menginput perintah "dl N" (Dengan N merupakan bilangan bulat jumlah gambar yang ingin didownload). Maka program akan membuat file dengan format YYYY-MM-DD_USERNAME terlebih dahulu apabila belum ada, apabila sudah ada, maka program akan mengunzip file tersebut dengan password akun tersebut, kemudian mentrack nama file terakhir yang diunduh. Setelah melewati proses tersebut, program akan menjalankan loop dari angka file terakhir (1 apabila belum ada) sampai ke angka file terakhir + jumlah filenya untuk melakukan pengunduhan. Pengunduhan menggunakan command wget. Setelah pengunduhan berhasil dijalankan, folder tempat gambar akan dizip dengan password yang sama dengan password user tersebut. 

```bash
if [[ ${cmd:0:2} == "dl" ]]; then
        #checking if pic folder exists
        if ! [[ -f "$folder_name.zip" ]]; then
                mkdir $folder_name
                curr_file=1
        else
                unzip -P $password -q "$folder_name.zip"
                rm -f "$folder_name.zip"
                curr_file=`ls $folder_name | sort -rn | head -1`
                curr_file=${curr_file:4}
                curr_file="$((curr_file + 1))"
        fi
        number_download=${cmd:3}
        for num in $(seq -f "%02g" $curr_file $((curr_file + number_downl>
        do
                wget -q --output-document=$folder_name/PIC_$num  https://>
        done
        zip -P $password -r "$folder_name.zip" $folder_name
        rm -r $folder_name
```
#### Opsi command setelah login :
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-56-00.png)
#### Isi zip file-file yang didownload :
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_10-59-49.png)
#### Download akan berlanjut apabila file sudah ada:
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_11-01-51.png)

Apabila user memilih opsi "att" maka akan digunakan awk untuk menghitung kemunculan kata "LOGIN:" pada log dan kemudian mengoutputnya di konsol.

Bila perintah yang diketik bukan termasuk dl N ataupun att maka program akan exit.

```bash
elif [[ $cmd == "att" ]]; then
        att=`awk -F'[ ]' ' BEGIN {count=0;} /alex/ { if ($3 == "LOGIN:") >
        echo "Total login attempt : $att"
else
        exit
fi
```
#### Output command att :
![](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-03-05_11-04-16.png)

2. Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:
- Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
- Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
- Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
- Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
- Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:
- File ratarata.txt
Rata-rata serangan adalah sebanyak rata_rata requests per jam

- File result.txt
IP yang paling banyak mengakses server adalah: ip_address sebanyak jumlah_request requests

Ada jumlah_req_curl requests yang menggunakan curl sebagai user-agent

IP Address Jam 2 pagi
IP Address Jam 2 pagi
dst


* Gunakan AWK
* Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder forensic_log_website_daffainfo_log


### Jawaban

Mula-mula akan dibuat folder dahulu bernama forensic_log_website_daffainfo_log, namun sebelumnya dicek terlebih dahulu apakah folder tersebut sudah dibuat sebelumnya.

```
path=forensic_log_website_daffainfo_log
if ! [[ -d $path ]]; then
	mkdir $path
fi

```
### Isi folder setelah script dijalankan
![](https://gitlab.com/bagusfebrian/tes/-/raw/main/ls.jpg)

Begitu juga dengan file lain yang harus dibuat yaitu file result.txt dan ratarata.txt
```
log=~/log_website_daffainfo.log
avg=ratarata.txt
res=result.txt

if ! [[ -f $path/$avg ]]; then
	touch $path/$avg
fi
if ! [[ -f $path/$res ]]; then
	touch $path/$res
fi

```

Selanjutnya mencari rata-rata request perjam yang dikirim. Karena rentang waktu request yang diterima antara jam 00.00-12.00 maka mencari rata-rata perjamnya akan diambil total seluruh request dan dibagi dengan 12 jam. Setelah itu akan dikirim ke file rata-rata.txt.

```
awk 'BEGIN{c1=0} 
//{c1++} 
END{print "Rata-rata serangan adalah sebanyak ",(c1-1)/12,"requests per jam"}' $log > $path/$avg

```
### Isi file rata-rata.txt
![](https://gitlab.com/bagusfebrian/tes/-/raw/main/avg.jpg "rata-rata.txt")

Untuk berikutnya akan dicari IP yang melakukan request terbanyak menggunakan awk. Dibuat array dengan menggunakan request sebagai index dan jumlah kemunculan request sebagai value array. Setelah itu dilakukan sort untuk mencari yang terbanyak dan kemudian diprint.

```
max_ip=$(awk -F: 'NR!=1{if($1)a[$1]++}END{for (i in a) {print i, a[i]}}' $log | sort -k 2n | tail -1 | awk '{print "IP yang paling banyak mengakses server adalah: "$1" sebanyak "$2" requests"}'
```

Selanjutnya akan dicari berapa request yang menggunakan user-agent **curl**. Digunakan awk untuk setiap **curl** yang keluar sebagai user-agent akan dilakukan increment dan kemudian diprint.

```
curl_req+=$(awk '/curl/ {++n}END {print "ada", n ,"requests yang menggunakan curl sebagai user-agent\n"}' $log)
```

Kemudian akan dicari semua daftar ip yang melakukan request  pada jam 2 pagi pada tanggal 22 menggunakan awk dengan kondisi dimana jam 02 pada tanggal 22 Januari.
```
twoam_ip+=$(awk -F:  '/22[/]Jan/{if($3=="02") print $1}' $log)
```

Kemudian hasil dari max_ip, curl_req, dan twoam_ip akan dimasukkan kedalam file result.txt yang sudah dibuat sebelumnya.
```
echo -e "$max_ip\n\n$curl_req\n\n$twoam_ip" > $path/$res
```
### Isi file result.txt
![](https://gitlab.com/bagusfebrian/tes/-/raw/main/res.jpg "result.txt")

3. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

   - Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
   - Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
   - Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
   - Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Note:
- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62

### jawaban

Mula-mula kita akan membuat file log yang akan memasukkan semua metric yang penamaannya sesuai dengan waktu saat file script bash tersebut dijalankan dengan format metrics_{YmdHms}.log. Dan akan mencatat metrics yang akan berjalan pada setiap menit.
```
if ! crontab -l | grep -q '~/soal-shift-sisop-modul-1-e04-2022/soal3/minute_log.sh'; then
	(crontab -l; echo "* * * * * ~/soal-shift-sisop-modul-1-e04-2022/soal3/minute_log.sh") | crontab -
	echo "not exists"
        fi
```
lalu, kita ngecek apakah folder log tersebut sudah ada atau belum
```
if ! [[ -d $log ]]; then
	mkdir $log
fi

```
lalu, melakukan monitoring ram dan size pada directory dengan menggukanakan awk lalu dimasukkan ke variabel path baru
```
path_size=$(du -sh $log | awk '{print  $1}')
path_log="$log/metrics_$(date +"%Y%m%d%H%M%S").log"

```
berikut contoh file log yang dibuat oleh crontab  di setiap menitnya

setelahnya, kita memberikan akses agar dapat mengeksekusi file log tersebut
### file log yang dihasilkan
![](https://gitlab.com/ardhanatio/kumpulan-file/-/raw/main/contoh_foto_log_metrics.png)
```
chmod +x ~/soal-shift-sisop-modul-1-e04-2022/soal3/minute_log.sh
```
memuat isi file metrics yang dijalankan tiap menit dan membaca nilainya dengan mengambil data dari file baris kedua, kolom dua sampai dengan tujuh serta mengambil data dari baris ketiga, kolom dua sampai dengan lima  lalu di direct ke file path log
```
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
$(free -m | awk 'NR == 2 {printf "%d,%d,%d,%d,%d,%d,", $2, $3, $4, $5, $6, $7} NR == 3 {printf "%d,%d,%d,%d,", $2, $3, $4, $5}')$log,$path_size" > $path_log

```
memebuat perintah agar yang dapat mengakses data yang telah dieksekusi hanya kita sendiri, sistem tidak dapat mengaksesnya
```
find $log -type f -exec chmod 600 {} \;
```
### tampilan izin mengeksekusi
![](https://gitlab.com/ardhanatio/kumpulan-file/-/raw/main/masukin_password.png)


selanjutnya kita akan membuat satu script  untuk membuat agregasi file log ke satuan jam. pertama tama kita mengecek apakah crontab untuk setiap jamnya sudah ada atau belum, jika belum maka secara otomatis akan langsung dibuat
```
if ! crontab -l | grep -q 'aggregate_minutes_to_hourly_log.sh'; then
        (crontab -l; echo "0 * * * * ~/soal-shift-sisop-modul-1-e04-2022/soal3/aggregate_minutes_to_hourly_log.sh") | crontab -
fi


```
membuat nama file dengan format nama sesuai waktu inputan, lalu membuat variabel t untuk menyimpan waktu jam saat file tersebut diakses
```
file_name="metrics_agg_$(date +"%Y%m%d%H").log"
t="metrics_$(date +%Y%m%d)"
h="$(date +%H)"
h=$((h-1))

```
membuat conditional function agar ketika nilai h yang didapatkan bernilai negatif dapat kita ubah menjadi positif dengan menggunakan fungsi matematis agar nantinya program dapat berjalan sesuai keinginan awal
```
if [[ $h -lt 0 ]]; then
	h+=24;
	h=$((h%24))
elif [[ $h -lt 10 ]]; then
	t+="0$h"
else
	t+=$h
fi

```
melakukan loop dan mendapatkan nilai maksimum, minimum, dan rata rata dari tiap-tiap metrics menggunakan $t* yang artinya menggunakan data waktu . lalu file tersebut akan ditrigger untuk berjalan setiap jam secara otomatis

```
 for i in {1..12}
do
	max[$i]=$(awk -F',' -v i=$i 'NR%2==0{print $i}' $log/$t*.log | sort -r | head -1)
	min[$i]=$(awk -F',' -v i=$i 'NR%2==0{print $i}' $log/$t*.log | sort | head -1)
	avg[$i]=$(awk -F',' -v i=$i 'NR%2==0{sum+=$i} END{print 2*sum/NR}' $log/$t*.log)
done

```
memasukkan nilai yang didapatkan di atas kedalam file log lalu membuat output yang berisi nilai nilai minimum, maksimum, dan rata rata  lalu menyimpannya ke dalam file log.
```
avg[11]=$log
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
minimum,$(printf '%s,' "${min[@]}")
maximum,$(printf '%s,' "${max[@]}")
average,$(printf '%s,' "${avg[@]}")" > "$log/$file_name"

find $log -type f -exec chmod 600 {} \;
```
berikut contoh output yang dihasilkan dalam mencari nilai minimum, maksimum, dan rata rata dari beberapa contoh file
### output mencari maksimum, minimum dan avarage
![](https://gitlab.com/ardhanatio/kumpulan-file/-/raw/main/metrics_ggreegate.png)



### Kesulitan
- Mencari regex yang tepat
- Mempelajari AWK
- Memikirkan cara tersingkat untuk mencari aggregat metrics pada nomor 3
