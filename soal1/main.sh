#!/bin/bash

log=log.txt
db=users/user.txt
success=0

#checking if log file exists
if ! [[ -f "$log" ]]; then
	touch "$log"
fi
#checking if db exists
if ! [[ -f "$db" ]]; then
	mkdir users
	touch "$db"
fi

msg="$(date +%D" "%T) "

echo "Input username to login : "
read  username
echo "Input password : "
read -s password

db_password=`awk -v name="$username" -F: '$1 ~ name {print $2}' $db`

if ! [[ $db_password == $password ]]; then
	msg+="LOGIN: ERROR Failed login attempt on user $username"
else
	msg+="LOGIN: INFO User $username logged in"
	success=1
fi

echo "$msg" >> $log
echo "$msg"

if [[ $success == 0 ]]; then
	exit
fi

echo "Enter command
- dl N (N = Jumlah gambar yang akan didownload)
- att (menghitung jumlah attempt login)"
read cmd

folder_name="$(date +"%F"_$username)"

if [[ ${cmd:0:2} == "dl" ]]; then
	#checking if pic folder exists
	if ! [[ -f "$folder_name.zip" ]]; then
        	mkdir $folder_name
		curr_file=1
	else
		unzip -P $password -q "$folder_name.zip"
		rm -f "$folder_name.zip"
		curr_file=`ls $folder_name | sort -rn | head -1`
		curr_file=${curr_file:4}
		curr_file="$((curr_file + 1))"
	fi
	number_download=${cmd:3}
	for num in $(seq -f "%02g" $curr_file $((curr_file + number_download - 1)))
	do
		wget -q --output-document=$folder_name/PIC_$num  https://loremflickr.com/320/240
	done
	zip -P $password -r "$folder_name.zip" $folder_name
	rm -r $folder_name
elif [[ $cmd == "att" ]]; then
	att=`awk -F'[ ]' ' BEGIN {count=0;} /alex/ { if ($3 == "LOGIN:") count+=1} END {print count}' log.txt`
	echo "Total login attempt : $att"
else
	exit
fi

