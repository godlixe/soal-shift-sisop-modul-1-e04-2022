#!/bin/bash

log=log.txt
db=users/user.txt

#checking if log file exists
if ! [[ -f "$log" ]]; then
	touch "$log"
fi
#checking if db exists
if ! [[ -f "$db" ]]; then
	mkdir users
	touch "$db"
fi

msg="$(date +%D" "%T) "
password=0

echo "Input username to register : "
read  username

while ! [[ "$password" =~ [[:alnum:]] && ${#password} -ge  8 && "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]]  && ($password != $username) ]];
do
	echo "Enter an 8+ character of alphanumeric password"
	read -s password
done

if grep -q "^$username" $db ; then
	msg+="REGISTER: ERROR User already exists"
else
	echo "$username:$password" >> $db
	msg+="REGISTER: INFO User $username registered successfully"

fi

echo "$msg" >> $log
echo "$msg"
